$(document).ready(function() {
    var $posts = $("#posts");
    var postTemplate = $("#postTemplate").html();

    function addpost(post) {
        $posts.append(Mustache.render(postTemplate, post));
    }
    $.ajax({

        type: 'GET',
        url: "https://jsonplaceholder.typicode.com/posts",
        success: function(posts) {
            $.each(posts, function(index, post) {
                addpost(post);
            });
            //console.log(data);
        },
        error: function() {
            alert("error loading posts");
        }
    });
});