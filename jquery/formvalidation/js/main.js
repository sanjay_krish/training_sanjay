$(document).ready(function() {

    $('.js-example-basic-single').select2();

    $("#date").change(function() {
        var dob = $('#date').val();
        var day, month, year;
        var date = new Date(dob);
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();
        var today = new Date();
        var age = Math.floor((today - date) / (365.25 * 24 * 60 * 60 * 1000));
        $("#age").val(age);
    });

    $('#submit').on('click', function() {

        var name = $("#name").val();
        var email = $("#email").val();
        var mobileNumber = $("#mobile_number").val();
        var country = $("#country").val();
        var dob = $('#date').val();
        var day, month, year;
        var date = new Date(dob);
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

        var numformat = /^\d{10}$/;
        var nameformat = /^[A-Za-z][A-Za-z ]+$/;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var ageformat = /^([1-9]|[1-9][0-9]|[1][0-1][1-9])$/;

        var flag = 1;

        if (name == "") {
            $("#err_name").css("color", "red").html("Name is required");
            flag = 0;
        } else {
            if (nameformat.test(name)) {
                $("#err_name").css("color", "green").html("Name is valid");
            } else {
                $("#err_name").css("color", "red").html("Name is Invalid");
                flag = 0;
            }
        }

        if (email == "") {
            $("#err_email").css("color", "red").html("Email is required");
            flag = 0;;
        } else {
            if (mailformat.test(email)) {
                $("#err_email").css("color", "green").html("Email is valid");
            } else {
                $("#err_email").css("color", "red").html("Email is Invalid");
                flag = 0;
            }
        }

        if (Number.isNaN(day) || Number.isNaN(month) || Number.isNaN(year)) {
            $("#err_date").css("color", "red").html("DOB is Invalid");
            flag = 0;
        } else {
            $("#err_date").css("color", "green").html("DOB is Valid");
        }

        if (mobileNumber == "") {
        	$("#err_mobile_number").css("display", "none")
        } else {
        	$("#err_mobile_number").css("display", "inline")
            if (numformat.test(mobileNumber)) {
                $("#err_mobile_number").css("color", "green").html("Mobilenumber is valid");

            } else {
                $("#err_mobile_number").css("color", "red").html("Mobilenumber is Invalid");
                flag = 0;
            }
        }

        if (country == null) {
            $("#err_country").css("color", "red").html("Country is required");
            flag = 0;
        } else {
            $("#err_country").css("color", "green").html("Country is valid");
        }

        if (flag == 1) { //location.reload();
            $("#message").css("color", "green").html("SUCCESFUL");
            alert("Your submission was successful");
            return true;

        }

        if (flag == 0) {

            $("#message").css("color", "red").html("* Mandatory Fields");
            return false;
        }
    });


    $("#reset").click(function() {
        location.reload();
    });
});