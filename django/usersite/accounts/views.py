from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.views.generic  import View
from django.views.generic.edit  import UpdateView
from django.contrib.auth.decorators import login_required
from .forms import UserForm,ProfileForm
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.auth.models import 
from django.views.generic.edit import FormView
# Create your views here.
class SignUpView(View):
	form_class=UserForm
	template_name="accounts/signup.html"

	def get(self,request):
		form=self.form_class(None)
		return render(request,self.template_name,{'form':form})

	def post(self,request):
		form=self.form_class(request.POST)
		if form.is_valid():
			user=form.save(commit=False)

			username=form.cleaned_data['username']
			password=form.cleaned_data['password']
			user.set_password(password)
			user.save()


			user=authenticate(username=username,password=password)

			if user is not None:

				if user.is_active:
					login(request,user)
					return redirect('accounts:profile')

		return render(request,self.template_name,{'form':form})

@login_required(login_url='accounts:login')
def profile_view(request):
	template_name="accounts/profile.html"
	return render(request,template_name) 

def logout_view(request):
	logout(request)
	return redirect('accounts:login')

class ProfileUpdate(LoginRequiredMixin,FormView):
	login_url='accounts:login'
	form_class =ProfileForm
	template_name="accounts/update.html"
	# success_url = 'accounts:profile'
	def get(self,request):
		form=self.form_class(instance=request.user)
		return render(request,self.template_name,{'form':form},)
	def post(self,request):
		form=self.form_class(request.POST,instance=request.user)
		if form.is_valid():
			form.save()
			return redirect("accounts:profile")		
		
