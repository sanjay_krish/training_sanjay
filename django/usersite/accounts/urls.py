from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
app_name="accounts"

urlpatterns = [
    path('signup/', views.SignUpView.as_view(),name="signup"),
    path('profile/', views.profile_view,name="profile"),
    path('profile/update/', views.ProfileUpdate.as_view(),name="profile_update"),
    path('', views.logout_view,name="logout"),
    # path('login/', views.login_view,name="login"),
    path('login/', auth_views.LoginView.as_view(template_name='accounts/login.html'),name="login"),
]