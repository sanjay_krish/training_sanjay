from django.shortcuts import render,redirect
from django.views import View
from .models import Post,Like
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import PostCreationForm
from django.utils import timezone
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


class AddPost(LoginRequiredMixin,View):
	login_url='accounts:login'
	form_class =PostCreationForm
	template_name="posts/posts.html"
	def get(self,request):
		post_list = Post.objects.values().order_by('-pub_date')		
		for post in post_list:
			post['author']=Post.objects.get(id=post['id']).author
			post['likecount']=Like.objects.filter(post=post['id']).count()
			likeduser=Like.objects.filter(post=post['id'],user=request.user)
			if likeduser.exists():
				post['do']="Unlike"
			else:
				post['do']="Like"
		form=self.form_class(instance=request.user)
		context={'form':form,'post_list':post_list ,}		
		return render(request,self.template_name,context)
	def post(self,request):
		title_text = request.POST.get('title_input')
		content_text=request.POST.get('content_input')
		response_data = {}
		post = Post(title=title_text,content=content_text, author=request.user)
		post.save()
		response_data['id']=post.id
		response_data['title'] = post.title
		response_data['content'] = post.content
		response_data['pub_date'] = post.pub_date.strftime('%B %d, %Y %I:%M %p')
		response_data['author'] = post.author.username
		
		return JsonResponse(response_data) 

class AddLike(LoginRequiredMixin,View):
	login_url='accounts:login'
	def post(self,request):		
		pid = request.POST.get('postid')
		like=Like(post=Post.objects.get(id=pid),user=request.user)
		print(like.post)
		print(like.user)
		likeduser=Like.objects.filter(post=pid,user=request.user)
		if likeduser.exists():			
			likeduser.delete()
			message = 'Like'
		else:			
			like.save()
			message = 'Unlike'

		total_likes=Like.objects.filter(post=pid).count()
		ctx = {'likes_count': total_likes, 'message': message}
		print(ctx)
		# use mimetype instead of content_type if django < 5
		return JsonResponse(ctx)

