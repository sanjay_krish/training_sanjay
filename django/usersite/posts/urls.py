from django.urls import path
from . import views


app_name="posts"

urlpatterns = [

    path('posts/', views.AddPost.as_view(),name="posts"),
    path('posts/likepost/', views.AddLike.as_view(),name="like"),
]