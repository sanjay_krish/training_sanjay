from django.contrib import admin
from .models import Post,Like
# Register your models here.

class PostAdmin(admin.ModelAdmin):
	search_fields = ['title']
	list_filter = ['pub_date']
	list_display = ('title', 'content', 'pub_date','author')

class LikeAdmin(admin.ModelAdmin):
	search_fields = ['post']
	list_filter = ['time']
	list_display = ('post', 'user', 'time')

admin.site.register(Post, PostAdmin)
admin.site.register(Like,LikeAdmin)