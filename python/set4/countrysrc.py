# Fetch image url, country name from the URL http://example.webscraping.com/ using beautifulsoup

from bs4 import BeautifulSoup
import requests
import time

for k in range(26):
    source = requests.get(
        "http://example.webscraping.com/places/default/index/"+str(k)).text

    soup = BeautifulSoup(source, "lxml")

    for i in soup.find_all("td"):
        name = i.div.a.text
        s = i.div.a.img["src"]
        print(name)
        print("example.webscraping.com"+s)
        print()
        time.sleep(0.25)

