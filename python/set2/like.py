# Let 'a' be the list of users who likes a post! I want to get displayed as below.
# eg 1 :-       a = []
# Output :      Nobody likes This
# eg 2 :-       a = ['Alice']
# Output :      Alice likes This
# eg 3 :-       a = ['Alice','Bob']
# Output :      Alice and Bob likes This
# eg 4 :-       a = ['Alice', 'Bob', 'Charls']
# Output :      Alice, Bob and Charls likes This
# eg 5 :-       a = ['Alice', 'Bob', 'Charls','Denny']
# Output :      Alice, Bob and 2 others likes This
# eg 6 :-       a = ['Alice', 'Bob', 'Charls','Denny','Emely']
# Output :      Alice, Bob and 3 others likes This
a=raw_input(Enter names seperated by space:).split()
l=len(a)
s=""
if(l==0):
	s="Nobody"
elif(l==1):
	s=a[0]
elif(l==2):
	s=a[0]+" and "+a[1]
elif(l==3):
	s=a[0]+", "+a[1]+" and "+a[2]
else:
	s=a[0]+", "+a[1]+" and "+str(l-2)+" others"
print(s+" likes this.")
