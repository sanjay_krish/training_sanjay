# Define a class which has at least two methods:
# getString: to get a string from console input
# printString: to print the string in upper case.
# Also please include simple test function to test the class methods.
class Display():
	def __init__(self):
		self.data = ""

	def getString(self):
		self.data=raw_input("Enter string:")

	def printString(self):
		print("Entered string is:"+self.data.upper())	

a=Display()
a.getString()
a.printString()