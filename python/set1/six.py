# Define a class named Circle which can be constructed by a radius.
# The Circle class has a method which can compute the area.
import math
class Circle():
	"""docstring for ClassName"""
	def __init__(self, radius):
		self.radius = radius

	def area(self):
		self.area=(math.pi)*(self.radius)**2
		return self.area

	def printarea(self):
		print("Area:"+str(self.area()))

r=int(raw_input("Enter radius:"))
c=Circle(r)
c.printarea()
