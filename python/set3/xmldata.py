# Fetch data from the URL https://www.w3schools.com/xml/simple.xml and print name, price of each item.
import urllib.request
from xml.etree import ElementTree
x=urllib.request.urlopen("https://www.w3schools.com/xml/simple.xml")

dom=ElementTree.parse(x)

foods=dom.findall("food")
for f in foods:
	name=f.find("name")
	price=f.find("price")
	print("Name:"+name.text)
	print("Price:"+price.text+"\n")
