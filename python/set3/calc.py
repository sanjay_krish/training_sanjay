# Create a class with functions to add, subtract, multiply and division.
# Also write unit test for each functions.

class Calculate():
	"""docstring for ClassName"""
	def __init__(self):
		pass

	def add(self,a,b):
		if(type(a) or type(b)) not in [int,float]:
			raise TypeError("Values can only be real numbers")
		return a+b

	def subtract(self,a,b):
		if(type(a) or type(b)) not in [int,float]:
			raise TypeError("Values can only be real numbers")
		return a-b

	def multiply(self,a,b):
		if(type(a) or type(b)) not in [int,float]:
			raise TypeError("Values can only be real numbers")
		return a*b

	def divide(self,a,b):
		if(type(a) or type(b)) not in [int,float]:
			raise TypeError("Values can only be real numbers")
		if b==0:
			raise ValueError("Cannot divide by zero!!")
		return a/float(b)



		