import unittest

from calc import Calculate

X=Calculate()
class TestCalculate(unittest.TestCase):
	
	def test_add(self):
		self.assertEqual(X.add(5,3),8)
		self.assertRaises(TypeError, X.add, "dsa","ad")
		self.assertRaises(TypeError, X.add, "dsa",5)
		self.assertRaises(TypeError, X.add, 7,"ad")
		self.assertAlmostEqual(X.add(0.2,0.4),0.6)


	def test_subtract(self):
		self.assertEqual(X.subtract(5,3),2)
		self.assertRaises(TypeError, X.subtract, "dsa","ad")
		self.assertRaises(TypeError, X.subtract, "dsa",5)
		self.assertRaises(TypeError, X.subtract, 7,"ad")
		self.assertAlmostEqual(X.add(0.2,0.4),0.6)

	def test_multiply(self):
		self.assertEqual(X.multiply(5,3),15)
		self.assertRaises(TypeError, X.multiply, "dsa","ad")
		self.assertRaises(TypeError, X.multiply, "dsa",5)
		self.assertRaises(TypeError, X.multiply, 7,"ad")
		self.assertAlmostEqual(X.add(0.2,0.4),0.6)

	def test_divide(self):
		self.assertEqual(X.divide(15,3),5)
		self.assertAlmostEqual(X.divide(9,2),4.50)
		self.assertRaises(TypeError, X.divide, "dsa","ad")
		self.assertRaises(TypeError, X.divide, "dsa",5)
		self.assertRaises(TypeError, X.divide, 7,"ad")
		self.assertRaises(ValueError,X.divide,5,0)


if __name__ == '__main__':
    unittest.main()