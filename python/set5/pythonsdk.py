# Create a Python SDK for few API’s mentioned here https://jsonplaceholder.typicode.com/
# Features:
# Post
# Get posts
# Get posts based on the user
# Get post detail
# Comment
# Get comments
# Get comments based on the post
# Write unittest for minimum two functions
import urllib.request
import json

class JsonApi(object):
    """docstring for Posts"""

    def __init__(self):
        pass

    def return_data(self,x):
        url = "https://jsonplaceholder.typicode.com"+x
        response = urllib.request.urlopen(url)
        data = json.loads(response.read().decode())
        print(json.dumps(data, indent=2))

    def get_posts(self):
        self.return_data("/posts")

    def get_user_posts(self, userid):
        if(type(userid)) not in [int]:
            raise TypeError("Values should be integer")
        self.return_data("/posts?userId="+str(userid))

    def get_post_detail(self, postid):
        if(type(postid)) not in [int]:
            raise TypeError("Values should be integer")
        self.return_data("/posts/"+str(postid))

    def get_comments(self):
        self.return_data("/comments")

    def get_post_comments(self, postid):
        if(type(postid)) not in [int]:
            raise TypeError("Values should be integer")
        self.return_data("/comments?postId="+str(postid))
		


