from pythonsdk import JsonApi

choice="y"

X=JsonApi()

while(choice=="y"):
	print("Menu:")
	print("1.Get posts")
	print("2.Get posts based on the user")
	print("3.Get post detail")
	print("4.Get comments")
	print("5.Get comments based on the post")

	option=input("Enter option number:")

	if(option=="1"):
		X.get_posts()
	elif(option=="2"):
		uid=input("Enter user id to search posts:")
		if(type(uid)) not in [int]:
			raise TypeError("Values can integer")
		X.get_user_posts(uid)
	elif(option=="3"):
		pid=input("Enter post id to get detail:")
		if(type(pid)) not in [int]:
			raise TypeError("Values can integer")
		X.get_post_detail(pid)
	elif(option=="4"):
		X.get_comments()
	elif(option=="5"):
		pid=input("Enter post id to get comments:")
		if(type(pid)) not in [int]:
			raise TypeError("Values should be integer")
		X.get_post_comments(pid)
	else:
		print("Invalid option!!")

	choice=input("Do you want to continue? (y/n):")