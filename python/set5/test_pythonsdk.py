import unittest
from pythonsdk import JsonApi

a=JsonApi()
class TestJsonApi(unittest.TestCase):
	def  test_get_user_posts(self):
		self.assertRaises(TypeError,a.get_user_posts,5)
	def test_get_post_comments(self):
		self.assertRaises(TypeError,a.get_post_comments,5)   

if __name__ == '__main__':
	unittest.main()